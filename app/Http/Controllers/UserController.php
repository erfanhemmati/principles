<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function increaseBalanceV1(Request $request, $id)
    {
        $amount         = $request->get('amount');
        $user           = User::query()->find($id);

//        // 1
//        $user->balance  = $user->balance + $amount;
//        $user->save();
//
//        // 2
//        $user->update(['balance' => $user->balance + $amount]);
//
//        // 3
//        $user->increment('balance', $amount);

        $user->increaseBalance($amount);

        return response()->json(['success' => true]);
    }

    public function increaseBalanceV2(Request $request, User $user)
    {
        $amount         = $request->get('amount');

//        // 1
//        $user->balance  = $user->balance + $amount;
//        $user->save();
//
//        // 2
//        $user->update(['balance' => $user->balance + $amount]);
//
//        // 3
//        $user->increment('balance', $amount);

        $user->increaseBalance($amount);

        return response()->json(['success' => true]);
    }
}
