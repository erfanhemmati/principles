<?php

namespace Src\Srp\Resolved1;

interface MailerInterface
{
    public function send(Message $message);
}
