<?php

namespace Src\Srp\Resolved1;

class User
{
    public function getConfirmCode(): string
    {
        return '1234';
    }

    public function getClientName(): string
    {
        return 'John Doe';
    }

    public function getEmail(): string
    {
        return 'john_doe@gmail.com';
    }
}
