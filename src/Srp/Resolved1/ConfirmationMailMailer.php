<?php

namespace Src\Srp\Resolved1;

class ConfirmationMailMailer
{
    public function __construct
    (
        private readonly ConfirmationMailFactory $confirmationMailFactory,  // instance variable
        private readonly MailerInterface $mailer                            // send an email to client.

    )
    {
        //
    }

    /**
     * Send an email to client via given message and content
     *
     * @param User $user
     * @return void
     */
    public function sendTo(User $user): void
    {
        $message = $this->generateMessage($user);
        $this->sendEmail($message);

        // $this->mailer->send($message);
    }

    /**
     * Generate an email content via context of factory class
     *
     * @param User $user
     * @return Message
     */
    private function generateMessage(User $user): Message
    {
        return $this->confirmationMailFactory->generateMessage($user);
    }

    /**
     * Send given email message/content to client
     *
     * @param Message $message
     * @return void
     */
    private function sendEmail(Message $message): void
    {
        $this->mailer->send($message);
    }
}
