<?php

namespace Src\Srp\Resolved1;

interface TemplateEngineInterface
{
    public function render(string $template, array $params): string;
}
