<?php

namespace Src\Srp\Resolved1;

class ConfirmationMailFactory
{
    /**
     * @param TranslatorInterface $translator
     * @param TemplateEngineInterface $templateEngine
     */
    public function __construct
    (
        private readonly TranslatorInterface $translator,
        private readonly TemplateEngineInterface $templateEngine
    )
    {
        //
    }

    /**
     * Generate an email content
     *
     * @param User $user
     * @return Message
     */
    public function generateMessage(User $user): Message
    {
        $subject = $this->translator->translate('Please confirm your email address.');
        $body    = $this->templateEngine->render('emails.confirmation', [
            'confirm_code' => $user->getConfirmCode(),
            'client_name'  => $user->getClientName()
        ]);
        $email   = $user->getEmail();

        return new Message($subject, $body, $email);
    }
}
