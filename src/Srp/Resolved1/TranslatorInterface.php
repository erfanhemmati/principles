<?php

namespace Src\Srp\Resolved1;

interface TranslatorInterface
{
    public function translate(string $text): string;
}
