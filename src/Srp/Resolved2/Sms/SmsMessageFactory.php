<?php

namespace Src\Srp\Resolved2\Sms;

use Src\Srp\Resolved2\MessageFactoryInterface;
use Src\Srp\Resolved2\User;

class SmsMessageFactory implements MessageFactoryInterface
{
    public function __construct
    (
        private readonly SmsTemplateInterface $smsTemplate
    )
    {
        //
    }

    public function generate(User $user): SmsMessage
    {
        $content      = $this->smsTemplate->render('client.confirmation', [
            'confirm_code'  => $user->getConfirmCode(),
            'client_name'   => $user->getClientName()
        ]);

        $mobile       = $user->getMobile();

        return new SmsMessage($mobile, $content);
    }
}
