<?php

namespace Src\Srp\Resolved2\Sms;

interface SmsTemplateInterface
{
    public function render(string $template, array $params);
}
