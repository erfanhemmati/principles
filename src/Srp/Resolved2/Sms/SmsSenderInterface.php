<?php

namespace Src\Srp\Resolved2\Sms;

interface SmsSenderInterface
{
    public function send(SmsMessage $message);
}
