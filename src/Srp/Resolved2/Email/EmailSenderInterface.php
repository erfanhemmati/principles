<?php

namespace Src\Srp\Resolved2\Email;

interface EmailSenderInterface
{
    public function send(EmailMessage $message);
}
