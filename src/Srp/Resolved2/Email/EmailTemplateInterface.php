<?php

namespace Src\Srp\Resolved2\Email;

interface EmailTemplateInterface
{
    public function render(string $template, array $params);
}
