<?php

namespace Src\Srp\Resolved2\Email;

use Src\Srp\Resolved2\User;
use Src\Srp\Resolved2\MessageFactoryInterface;

class EmailMessageFactory implements MessageFactoryInterface
{
    public function __construct
    (
        private readonly EmailTemplateInterface $emailTemplate
    )
    {
        //
    }

    public function generate(User $user): EmailMessage
    {
        $content      = $this->emailTemplate->render('emails.confirm', [
            'confirm_code' => $user->getConfirmCode(),
            'client_name'  => $user->getClientName()
        ]);

        $email        = $user->getEmail();

        return new EmailMessage($email, $content, 'Please confirm your email!');
    }
}
