<?php

namespace Src\Srp\Resolved2\Email;

class EmailMessage
{
    public function __construct
    (
        private readonly string $email,
        private readonly string $content,
        private readonly string $subject

    )
    {
        //
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }
}
