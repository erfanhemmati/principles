<?php

namespace Src\Srp\Resolved2;

use Src\Srp\Resolved2\Sms\SmsMessage;
use Src\Srp\Resolved2\Email\EmailMessage;
use Src\Srp\Resolved2\Sms\SmsMessageFactory;
use Src\Srp\Resolved2\Sms\SmsSenderInterface;
use Src\Srp\Resolved2\Email\EmailMessageFactory;
use Src\Srp\Resolved2\Email\EmailSenderInterface;

class ConfirmNotificationSender
{
    public function __construct
    (
        private readonly EmailSenderInterface $emailSender,
        private readonly SmsSenderInterface $smsSender,
        private readonly EmailMessageFactory $emailMessageFactory,
        private readonly SmsMessageFactory $smsMessageFactory
    )
    {
        //
    }

    public function sendTo(User $user): void
    {
        $this->sendEmail($this->emailMessageFactory->generate($user));

        $this->sendSms($this->smsMessageFactory->generate($user));
    }

    private function sendEmail(EmailMessage $message): void
    {
        $this->emailSender->send($message);
    }

    public function sendSms(SmsMessage $message): void
    {
        $this->smsSender->send($message);
    }
}
