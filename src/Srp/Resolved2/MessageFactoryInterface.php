<?php

namespace Src\Srp\Resolved2;

interface MessageFactoryInterface
{
    public function generate(User $user);
}
