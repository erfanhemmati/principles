<?php

namespace Src\Srp\Problem2\Sms;

interface SmsTemplateInterface
{
    public function render(string $template, array $params);
}
