<?php

namespace Src\Srp\Problem2\Sms;

class SmsMessage
{
    public function __construct
    (
        private readonly string $mobile,
        private readonly string $content
    )
    {
        //
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
