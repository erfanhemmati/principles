<?php

namespace Src\Srp\Problem2\Sms;

interface SmsSenderInterface
{
    public function send(SmsMessage $message);
}
