<?php

namespace Src\Srp\Problem2;

use Src\Srp\Problem2\Sms\SmsMessage;
use Src\Srp\Problem2\Email\EmailMessage;
use Src\Srp\Problem2\Sms\SmsSenderInterface;
use Src\Srp\Problem2\Sms\SmsTemplateInterface;
use Src\Srp\Problem2\Email\EmailSenderInterface;
use Src\Srp\Problem2\Email\EmailTemplateInterface;

class ConfirmNotificationSender
{
    public function __construct
    (
        private readonly EmailSenderInterface $emailSender,
        private readonly SmsSenderInterface $smsSender,
        private readonly EmailTemplateInterface $emailTemplate,
        private readonly SmsTemplateInterface $smsTemplate
    )
    {
        //
    }

    public function sendTo(User $user): void
    {
        $emailMessage = $this->generateEmailMessage($user);
        $this->sendEmail($emailMessage);

        $smsMessage   = $this->generateSmsMessage($user);
        $this->sendSms($smsMessage);
    }

    private function generateEmailMessage(User $user): EmailMessage
    {
        $content      = $this->emailTemplate->render('emails.confirm', [
            'confirm_code' => $user->getConfirmCode(),
            'client_name'  => $user->getClientName()
        ]);

        $email        = $user->getEmail();

        return new EmailMessage($email, $content, 'Please confirm your email!');
    }

    private function generateSmsMessage(User $user): SmsMessage
    {
        $content      = $this->smsTemplate->render('client.confirmation', [
            'confirm_code'  => $user->getConfirmCode(),
            'client_name'   => $user->getClientName()
        ]);

        $mobile       = $user->getMobile();

        return new SmsMessage($mobile, $content);
    }

    private function sendEmail(EmailMessage $message): void
    {
        $this->emailSender->send($message);
    }

    public function sendSms(SmsMessage $message): void
    {
        $this->smsSender->send($message);
    }
}
