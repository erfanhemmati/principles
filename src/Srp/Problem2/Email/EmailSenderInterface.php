<?php

namespace Src\Srp\Problem2\Email;

interface EmailSenderInterface
{
    public function send(EmailMessage $message);
}
