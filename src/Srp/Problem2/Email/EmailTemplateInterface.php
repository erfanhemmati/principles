<?php

namespace Src\Srp\Problem2\Email;

interface EmailTemplateInterface
{
    public function render(string $template, array $params);
}
