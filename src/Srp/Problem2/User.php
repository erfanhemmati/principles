<?php

namespace Src\Srp\Problem2;

class User
{
    public function getConfirmCode(): string
    {
        return '1234';
    }

    public function getClientName(): string
    {
        return 'John Doe';
    }

    public function getMobile(): string
    {
        return "+989146931409";
    }

    public function getEmail(): string
    {
        return "john_doe@gmail.com";
    }

}
