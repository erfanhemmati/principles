<?php

namespace Src\Srp\Problem1;

interface MailerInterface
{
    public function send(Message $message);
}
