<?php

namespace Src\Srp\Problem1;

interface TemplateEngineInterface
{
    public function render(string $template, array $params): string;
}
