<?php

namespace Src\Srp\Problem1;

class Message
{
    public function __construct
    (
        private readonly string $subject,
        private readonly string $body,
        private readonly string $email
    )
    {
        //
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
