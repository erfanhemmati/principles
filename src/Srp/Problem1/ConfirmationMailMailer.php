<?php

namespace Src\Srp\Problem1;

class ConfirmationMailMailer
{
    /**
     * @param TemplateEngineInterface $templateEngine
     * @param TranslatorInterface $translator
     * @param MailerInterface $mailer
     */
    public function __construct
    (
        private readonly TemplateEngineInterface $templateEngine,   // render template like blade or twig.
        private readonly TranslatorInterface $translator,           // translate a content and body.
        private readonly MailerInterface $mailer                    // send an email to client.

    )
    {
        //
    }

    /**
     * Send an email to client via given message and content
     *
     * @param User $user
     * @return void
     */
    public function sendTo(User $user): void
    {
        $message = $this->generateMessage($user);
        $this->sendEmail($message);

        // $this->mailer->send($message);
    }

    /**
     * Generate an email content
     *
     * @param User $user
     * @return Message
     */
    private function generateMessage(User $user): Message
    {
        $subject = $this->translator->translate('Please confirm your email address.');
        $body    = $this->templateEngine->render('emails.confirmation', [
            'confirm_code' => $user->getConfirmCode(),
            'client_name'  => $user->getClientName()
        ]);
        $email   = $user->getEmail();

        return new Message($subject, $body, $email);
    }

    /**
     * Send given email message/content to client
     *
     * @param Message $message
     * @return void
     */
    private function sendEmail(Message $message): void
    {
        $this->mailer->send($message);
    }
}
