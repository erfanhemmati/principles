<?php

namespace Src\Srp\Problem1;

interface TranslatorInterface
{
    public function translate(string $text): string;
}
